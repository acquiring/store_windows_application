﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows;

namespace Store {
    class utility {
        public static MainWindow mainWindow;
        public static Plugins plugins = new Plugins();
        /// <summary>
        /// Associate File extension with exe
        /// </summary>
        /// <param name="Extension">extension string without .(dot) </param>
        /// <param name="RunWith"></param>
        public static void AssociateExtension(String Extension, String RunWith) {
            try {
                String ExtensionName = Extension.ToUpper();
                Registry.ClassesRoot.CreateSubKey("." + Extension).SetValue("", ExtensionName, RegistryValueKind.String);
                //Registry.ClassesRoot.CreateSubKey(ExtensionName + @"\shell\open\command").SetValue("", RunWith, RegistryValueKind.String);
                SetAssociation("." + Extension, ExtensionName, RunWith, "AcquiRing Plugin");
            } catch {
                MessageBox.Show("you need administrator permission");
            }
        }

        public static void SetAssociation(string Extension, string KeyName, string OpenWith, string FileDescription) {
            // The stuff that was above here is basically the same
            RegistryKey BaseKey;
            RegistryKey OpenMethod;
            RegistryKey Shell;
            RegistryKey CurrentUser;

            BaseKey = Registry.ClassesRoot.CreateSubKey(Extension);
            BaseKey.SetValue("", KeyName);

            OpenMethod = Registry.ClassesRoot.CreateSubKey(KeyName);
            OpenMethod.SetValue("", FileDescription);
            OpenMethod.CreateSubKey("DefaultIcon").SetValue("", "\"" + OpenWith + "\",0");
            Shell = OpenMethod.CreateSubKey("Shell");
            Shell.CreateSubKey("edit").CreateSubKey("command").SetValue("", "\"" + OpenWith + "\"" + " \"%1\"");
            Shell.CreateSubKey("open").CreateSubKey("command").SetValue("", "\"" + OpenWith + "\"" + " \"%1\"");
            BaseKey.Close();
            OpenMethod.Close();
            Shell.Close();


            // Delete the key instead of trying to change it
            CurrentUser = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts\\" + Extension, true);
            if (CurrentUser != null) {
                CurrentUser.DeleteSubKey("UserChoice", false);
                CurrentUser.Close();
            }

            // Tell explorer the file association has been changed
            SHChangeNotify(0x08000000, 0x0000, IntPtr.Zero, IntPtr.Zero);
        }

        [DllImport("shell32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern void SHChangeNotify(uint wEventId, uint uFlags, IntPtr dwItem1, IntPtr dwItem2);

        public static void UpdateJsonPluginFile() {
            List<API> apis = new List<API>();
            foreach (Plugin p in plugins) {
                if (p.isInstalled()) {
                    apis.AddRange(p.getAPIS());
                }
            }
            String path = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            String JsonPath = Path.Combine(path, "ACQUIRING_APIS.json");
            string json = JsonConvert.SerializeObject(apis);
            File.WriteAllText(JsonPath, json);
        }
    }
}
