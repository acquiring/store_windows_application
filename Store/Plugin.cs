﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.IO.Compression;
using System.Windows.Input;
using Newtonsoft.Json;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Store {
    public class Plugins : ObservableCollection<Plugin> {
        public Plugins() {
        }
    }
    public class Plugin {
        public Plugin() {
            progressBarValue = 0;
        }
        public int progressBarValue { get; set; }
        public string StoreID { get; set; }
        public string Name { get; set; }
        public string DownloadLink { get; set; }
        public string Description { get; set; }
        public BitmapImage bitmap { get; set; }
        private ICommand _installCommand, _uninstallCommand;
        public bool isInstalled() {
            if (Directory.Exists(getDirectory()) && File.Exists(getExecutionFilePath()) && File.Exists(getJsonFilePath())) {
                return true;
            } else {
                return false;
            }
        }
        public string getDirectory() {
            return "plugins\\" + StoreID + "\\";
        }
        public string getJsonFilePath() {
            return getDirectory() + "APIS.json";
        }
        public string getExecutionFilePath() {
            return getDirectory() + "Execute.exe";
        }
        public bool isNotinstalled() {
            return !isInstalled();
        }
        public void uninstall() {
            try {
                string PluginFolder = getDirectory();
                System.IO.Directory.Delete(PluginFolder, true);
                utility.UpdateJsonPluginFile();
                utility.mainWindow.UpdateUI();
            } catch { }
        }
        public void install() {
            try {
                if (isInstalled()) {
                    return;
                } else {
                    string PluginFolder = getDirectory();
                    System.IO.Directory.CreateDirectory(PluginFolder);
                    WebClient client = new WebClient();
                    string zipPath = @"plugins\" + StoreID + ".zip";
                    var zipPathUri = new Uri(DownloadLink);
                    if (File.Exists(zipPath)) File.Delete(zipPath);
                    if (Directory.Exists(PluginFolder)) System.IO.Directory.Delete(PluginFolder, true);
                    client.DownloadFileCompleted += (sender, e) => {
                        ZipFile.ExtractToDirectory(zipPath, PluginFolder);
                        File.Delete(zipPath);
                        String EXEPath = Path.Combine(Directory.GetCurrentDirectory(), getExecutionFilePath());
                        utility.AssociateExtension(getExtension(), EXEPath);
                        utility.UpdateJsonPluginFile();
                        utility.mainWindow.UpdateUI();
                    };
                    client.DownloadFileAsync(zipPathUri, zipPath);
                }
            } catch { }
        }
        public string getExtension() {
            return "acquiring_" + StoreID;
        }
        public List<API> getAPIS() {
            List<API> apis = new List<API>();
            try {
                apis = JsonConvert.DeserializeObject<List<API>>(File.ReadAllText(getJsonFilePath()));
            } catch {
                MessageBox.Show("problem when reading Plugin APIs");
            }
            foreach (API api in apis) {
                api.Exetnsion = getExtension();
            }
            return apis;
        }
        public ICommand installCommand {
            get {
                if (_installCommand == null) {
                    _installCommand = new RelayCommand(param => this.install(), param => isNotinstalled());
                }
                return _installCommand;
            }
        }
        public ICommand uninstallCommand {
            get {
                if (_uninstallCommand == null) {
                    _uninstallCommand = new RelayCommand(param => this.uninstall(), param => isInstalled());
                }
                return _uninstallCommand;
            }
        }
    }
}
