﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Store {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
            List<Plugin> data = GetStoreJson();
            utility.plugins = new Plugins();
            utility.mainWindow = this;
            foreach (Plugin p in data) { utility.plugins.Add(p); }
            FileList.ItemsSource = utility.plugins;
        }
        public void UpdateUI() {
            this.UpdateLayout();
        }
        public List<Plugin> GetStoreJson() {
            using (WebClient client = new WebClient()) {
                List<Plugin> Plugins = new List<Plugin>();
                String StoreData;
                try {
                    StoreData = client.DownloadString("http://madel0093-001-site1.etempurl.com/plugins.php");
                } catch {
                    MessageBox.Show("Error Downloading Plugins` list Please check your internet connection");
                    return Plugins;
                }

                try {
                    Plugins = JsonConvert.DeserializeObject<List<Plugin>>(StoreData);
                } catch {
                    MessageBox.Show("Error while parsing the json file, please try again later");
                }
                return Plugins;
            }
        }

    }
}
